//
//  TutorialVC.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 27/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var welcomeTextView: UILabel!
    @IBOutlet weak var addNewTaskImage: UIImageView!
    @IBOutlet weak var addNewTaskLabel: UILabel!
    @IBOutlet weak var swipeImage: UIImageView!
    @IBOutlet weak var swipeLabel: UILabel!
    @IBOutlet weak var newSubTaskImage: UIImageView!
    @IBOutlet weak var newSubTaskLabel: UILabel!
    @IBOutlet weak var subtaskListImage: UIImageView!
    @IBOutlet weak var subtaskListLabel: UILabel!
    @IBOutlet weak var toWorkButton: UIButton!
    
    // TO-DO:
    // - rename buttons in navigation stack
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func toWorkButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let locateLanguage = Locale.current.languageCode
        switch locateLanguage {
        case "en":
            addNewTaskImage.image = UIImage(named: "First_us")
            swipeImage.image = UIImage(named: "Second_us")
            newSubTaskImage.image = UIImage(named: "Third_us")
            subtaskListImage.image = UIImage(named: "Fourth_us")
        case "ru":
            addNewTaskImage.image = UIImage(named: "First_ru")
            swipeImage.image = UIImage(named: "Second_ru")
            newSubTaskImage.image = UIImage(named: "Third_ru")
            subtaskListImage.image = UIImage(named: "Fourth_ru")
        default :
            addNewTaskImage.image = UIImage(named: "First_us")
            swipeImage.image = UIImage(named: "Second_us")
            newSubTaskImage.image = UIImage(named: "Third_us")
            subtaskListImage.image = UIImage(named: "Fourth_us")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "Tutorial was see") == false {
           UserDefaults.standard.set(true, forKey: "Tutorial was see")
        }
    }
}
