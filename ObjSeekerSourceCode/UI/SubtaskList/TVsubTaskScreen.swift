//
//  TVsubTask.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 11/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//

import UIKit
import CoreData

// Окно со списком подзадач, содержит список подзадач отсортированных в соотвествтии со связью с задачей (с помощью выборки через tableSubTask.getSubTaskForTask(parent))
// Так же реализован функционал удаления, редактирования и создания объектов сущности SubTask
class TVsubTask: UIViewController {
    
    let dataProvider: SubtasksDataProvider?
    let navigation: Navigation?
    private var sortingData: SortingData?
    
//    MARK: - View Lifecycle
    
    init(provider: SubtasksDataProvider?, router: Navigation) {
        dataProvider = provider
        navigation = router
        sortingData = SortingData(array: nil)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("TVsubTask screen deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // В случае если окно было открыто без входного параметра, вывести ошибку таблицы
        if dataProvider?.parentTask == nil {
            let alert = UIAlertController(title: S.Alert.STError, message: S.Alert.TError, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: S.Alert.Ok, style: .cancel)
            alert.addAction(cancelAction)
            present(alert, animated: true)
        }
        if let task = dataProvider?.parentTask {
            dataProvider?.subtaskController = SubTasks.getSubTaskForTask(identifier: task)
            dataProvider?.subtaskController?.delegate = dataProvider
            dataProvider?.subtaskController?.fetchRequest.sortDescriptors = CoreDataManager.sortKey
            dataProvider?.subtaskController = CoreDataManager.instance.fetchingData(requestController: dataProvider?.subtaskController! as! FetchedResult) as? FetchedSubTasks
        }
        dataProvider?.tableView?.sectionHeaderHeight = UITableView.automaticDimension
        dataProvider?.tableView?.estimatedSectionHeaderHeight = 40
        dataProvider?.tableView?.tableFooterView = UIView()

        let gradient = LayerForViews.gradiented(toView: view, colors: [Style.UserColors.bgTop.cgColor, Style.UserColors.bgBottom.cgColor])
        view.layer.insertSublayer(gradient, at: 0)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cellTapped(with:)),
            name: NSNotification.Name(StcStr.Notif.SLV.detailAction),
            object: nil
        )
        
        setupNavBar()
        settingTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataProvider?.tableView?.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let gradientLayer = view.layer.sublayers?[0] as? CAGradientLayer else { return }
        gradientLayer.frame = view.bounds
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
//    MARK: - Custom Functions
    
    private func settingTable() {
        if let tableView = dataProvider?.tableView {
            self.view.addSubview(tableView)
            tableView.anchor(
                top: view.safeAreaLayoutGuide.topAnchor,
                leading: view.leadingAnchor,
                bottom: view.bottomAnchor,
                trailing: view.trailingAnchor
            )
        }
    }
    
    @objc func cellTapped(with notification: Notification) {
        guard let userInfo = notification.userInfo, let task = userInfo["data"] as? SubTasks else { return }
        navigation?.pushDetailScreen(task: task)
    }
    
    func setupNavBar() {
        let addTaskButton = UIBarButtonItem(
            image: StcImage.addIcon,
            landscapeImagePhone: StcImage.addIcon,
            style: .plain,
            target: self,
            action: #selector(addButtonPressed)
        )
        let sortButton = UIBarButtonItem(
            image: StcImage.sortIcon,
            landscapeImagePhone: StcImage.sortIcon,
            style: .plain,
            target: self,
            action: #selector(sortButtonPressed)
        )
        
        navigationItem.rightBarButtonItems = [addTaskButton, sortButton]
        navigationItem.title = S.NavBar.SubTaskList
        navigationItem.largeTitleDisplayMode = .always
    }
    
//    MARK: - Notification Actions
    @objc func addButtonPressed() {
        AlertTextFieldController.createAndShow(
            parent: self,
            alertType: .alert,
            placeholder: .tvSubTask,
            title: S.Alert.NewSubTask, actionHandler: { [weak self] text in
                guard let task = self?.dataProvider?.parentTask else { return }
                guard let unwrappedText = text, unwrappedText != "" else { return }
                let newSubtask = SubTasks()
                newSubtask.nameString = unwrappedText
                newSubtask.parent = task
                self?.dataProvider?.parentTask?.isAchieved = false
                CoreDataManager.instance.saveContext()
        })
    }
    
    @objc func sortButtonPressed() {
        if let sortArray = sortingData {
            AlertSortController.createAndShow(
                parent: self,
                alertType: .sheet,
                placeholder: .tvTask,
                title: S.Sheet.SortBy,
                sortingArray: sortArray.descriptors,
                sortBy: sortArray.currentState,
                actionHandler: { [weak self] (newState) in
                    guard let newState = newState else { return }
                    let newSort = SortingData(state: newState, array: sortArray.descriptors)
                    self?.sortingData = newSort
                    self?.dataProvider?.changeSorting(By: newSort.descriptors)
                }
            )
        }
    }
}






