//
//  SubtasksDataProvider.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 02/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import CoreData

class SubtasksDataProvider: NSObject {
    
//     MARK: - Properties
    
    /// Передаваемый параметр из предыдущего окна (родитель для подзадачи)
    weak var parentTask: Task?
    /// Отсортированный запрос в Core Data по задачам
    var subtaskController: FetchedSubTasks?
    var tableView: UITableView?
    
//    MARK: - Custom function
    
    func changeSorting(By sort: [NSSortDescriptor]) {
        guard let subTask = subtaskController else { return }
        subTask.fetchRequest.sortDescriptors = sort
        subtaskController = CoreDataManager.instance.fetchingData(requestController: subTask as! FetchedResult) as? FetchedSubTasks
        
        tableView?.beginUpdates()
        tableView?.reloadData()
        tableView?.endUpdates()
    }
    
    private func setTableView() {
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.register(CustomSubTaskCell.self, forCellReuseIdentifier: CustomSubTaskCell.reuseIdentifier)
        tableView?.backgroundColor = .clear
        tableView?.separatorStyle = .none
    }
    
//    MARK: Object Lifecycle
    
    init(initValue: Task) {
        self.parentTask = initValue
        tableView = UITableView()
        super.init()
        subtaskController?.delegate = self
        setTableView()
    }
    
    deinit {
        tableView?.removeFromSuperview()
        tableView = nil
        subtaskController = nil
    }
}

//    MARK: - UITableViewDelegate

extension SubtasksDataProvider: UITableViewDelegate {
    
    // Эта функция необходима для создания достаточного пространства заголовку таблицы с именем задачи, чтобы она не обрезалась. Это осуществляется за счет создания UIView, в котором создается label с текстом нужных размеров
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        // TODO: Проверить если ли здесь утечка
        // Программно создаем View  для section в TableView
        if section == 0 {
            let headerLabel = UILabel()
            headerLabel.text = parentTask?.nameString
            headerLabel.numberOfLines = 0
            headerLabel.sizeToFit()
            headerLabel.textAlignment = .center
            headerLabel.font = Style.UserFonts.taskMainFont
            headerLabel.textColor = Style.UserColors.borderForCells
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            headerView.addSubview(headerLabel)
            
            // Добавляем привязку к границам subview
            
            headerLabel.fillSuperview() // superview is headerView
            headerView.backgroundColor = Style.UserColors.cellUndoneColor
            headerView.layer.cornerRadius = 4
            headerView.layer.borderWidth = 1.5
            headerView.layer.borderColor = Style.UserColors.borderForCells.cgColor
            
            return headerView
        }
        return headerView
    }
    
    // На свайп появляется меню, в котором можно удалить ячейку или перейти в настройку соответствующей ей задачи
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: S.Actions.Delete) { [weak self] (_,_, result) in
            
            let subTask = self?.subtaskController?.object(at: indexPath)
            if let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: subTask?.deadlineIdentifier) {
                ReminderManager.instance.removeReminder(userReminder: dependReminder)
            }
            
            if let tableSubTask = self?.subtaskController as? FetchedResult {
                CoreDataManager.instance.deleteObject(at: indexPath, from: tableSubTask)
            }
            
            if let task = self?.parentTask, CoreDataManager.instance.checkAchievParent(withTask: task) {
                self?.parentTask?.isAchieved = true
            }
            CoreDataManager.instance.saveContext()
            result(true)
        }
        deleteAction.image = StcImage.deleteIcon
        deleteAction.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: S.Actions.Edit) { [weak self] (_,_, result) in
            let subtask = self?.subtaskController?.object(at: indexPath)
            NotificationCenter.default.post(
                name: NSNotification.Name(StcStr.Notif.SLV.detailAction),
                object: nil,
                userInfo: ["data" : subtask as Any]
            )
            result(true)
        }
        editAction.image = StcImage.editIcon
        editAction.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
    // Функция выделения завершенной/незавершенной задачи
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedRow = tableView.cellForRow(at: indexPath) as? CustomSubTaskCell {
            if selectedRow.nameStringLabel.textColor == UIColor.darkText {
                selectedRow.nameStringLabel.textColor = UIColor.gray
            } else {
                selectedRow.nameStringLabel.textColor = UIColor.darkText
            }
        }
        
        // Вносим изменения в объект, который находится в нажатой (выделенной) ячейке
        let subTask = subtaskController!.object(at: indexPath)
        subTask.isAchieved.toggle()
        
        // Проверяем на выполнение условия задачи (если все подзадачи выполнены, то и задача выполнена)
        if !(CoreDataManager.instance.checkAchievParent(withTask: parentTask!)) {
            parentTask?.isAchieved = false
        } else {
            parentTask?.isAchieved = true
        }
        CoreDataManager.instance.saveContext()
        self.tableView?.deselectRow(at: indexPath, animated: true)
    }
}

//    MARK: - UITableView DataSource

extension SubtasksDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let countSubtasks = subtaskController?.fetchedObjects?.count else { return 0 }
        return countSubtasks
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CustomSubTaskCell.reuseIdentifier, for: indexPath) as? CustomSubTaskCell {
            guard let subTask = subtaskController?.object(at: indexPath) else { return cell }
            cell.fillWithContent(with: subTask)
            
            return cell
        }
        return UITableViewCell()
    }
}

//    MARK: - Fetched Results Controller Delegate

extension SubtasksDataProvider: NSFetchedResultsControllerDelegate {
    // Данная часть отвечает за изменение tableView в случае, если были внесены изменения в объекты выводимые на экран
    
    func controllerWillChangeContent(_ controller: FetchedResult) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: FetchedResult, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView?.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                let subTask = subtaskController!.object(at: indexPath)
                let cell = tableView?.cellForRow(at: indexPath) as? CustomSubTaskCell
                cell?.nameStringLabel.text = subTask.nameString
                if (ReminderManager.instance.checkCalendarAccess()) && (subTask.deadlineIdentifier != nil) {
                    cell?.dateStringLabel.isHidden = false
                    cell?.dateStringLabel.text = subTask.dateForLabel
                } else { cell?.dateStringLabel.isHidden = true }
            }
        case .move:
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView?.insertRows(at: [newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: .automatic)
            }
        @unknown default:
            print("\(self): \(#function) Unknown default NSFetchedResultsChangeType")
        }
    }
    
    func controllerDidChangeContent(_ controller: FetchedResult) {
        tableView?.endUpdates()
    }
}
