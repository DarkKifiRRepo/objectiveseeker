//
//  CustomSubTaskCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 20/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//

import UIKit

// TODO: Сломаны констрейнты у date label найти и починить
class CustomSubTaskCell: UITableViewCell {
    
//     MARK: - Properties
    
    static let reuseIdentifier = "CustomSubTaskCell"
    
    let nameStringLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = Style.UserFonts.taskMainFont
        label.numberOfLines = 3
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.accessibilityIdentifier = "nameStringLabel_CustomSubTaskCell"
        return label
    }()
    
    let dateStringLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = Style.UserFonts.taskAddFont
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.accessibilityIdentifier = "dateStringLabel_CustomSubTaskCell"
        return label
    }()
    
    let contentTaskView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 8
        view.borderColor = Style.UserColors.borderForCells
        view.borderWidth = 2
        view.accessibilityIdentifier = "contentTaskView_CustomSubTaskCell"
        return view
    }()
    
    let stackView: UIStackView = {
        let st = UIStackView()
        st.alignment = .center
        st.axis = .vertical
        st.translatesAutoresizingMaskIntoConstraints = false
        st.accessibilityIdentifier = "stackView_CustomSubTaskCell"
        return st
    }()
    
//     MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        selectionStyle = .none
        setupBaseViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

//     MARK: - custom functions
    func fillWithContent(with subtask: SubTasks) {
        nameStringLabel.text = subtask.nameString
        
        if (subtask.deadlineIdentifier != nil) && (ReminderManager.instance.checkCalendarAccess()) {
            subtask.compareAchievedTaskAndReminder()
            dateStringLabel.text = subtask.dateForLabel
            setsContentWithTime()
        } else {
            setsContentWithoutTime()
        }
        
        if subtask.isAchieved {
            nameStringLabel.textColor = Style.UserColors.textComplete
            contentTaskView.backgroundColor = Style.UserColors.cellDoneColor
        } else {
            nameStringLabel.textColor = Style.UserColors.textColor
            contentTaskView.backgroundColor = Style.UserColors.cellUndoneColor
        }
    }
    
//    MARK: Layout for cell
    
    private func setupBaseViews() {
        contentView.addSubview(contentTaskView)
        contentTaskView.addSubview(stackView)
        
        contentTaskView.fillSuperview(padding: UIEdgeInsets(top: 4, left: 2, bottom: 4, right: 2))
        stackView.anchor(
            top: contentTaskView.topAnchor,
            leading: contentTaskView.leadingAnchor,
            bottom: contentTaskView.bottomAnchor,
            trailing: contentTaskView.trailingAnchor,
            padding: UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        )
    }
    
    private func setsContentWithoutTime() {
        stackView.addArrangedSubview(nameStringLabel)
    }
    
    private func setsContentWithTime() {
        stackView.addArrangedSubview(nameStringLabel)
        stackView.addArrangedSubview(dateStringLabel)
    }
}
