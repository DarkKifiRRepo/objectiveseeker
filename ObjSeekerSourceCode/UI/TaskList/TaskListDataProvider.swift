//
//  TaskListDataProvider.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 30/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import CoreData

class TaskListDataProvider: NSObject {
    
    //     MARK: - Properties
    
    // Флаг отвечает за вывод выполненных задач в tableView
    var showCompleteTask = ShowingStatus.show
    
    var fetchedResultController: FetchedTasks?
    let tableView: UITableView?
    
    //    MARK: Object Lifecycle
    
    override init() {
        // Получаем список задач
        fetchedResultController = {
            var result: NSFetchedResultsController<Task>  = CoreDataManager.instance.fetchedTaskController(arrayForSort: CoreDataManager.sortKey)
            result = CoreDataManager.instance.fetchingData(requestController: result as! FetchedResult) as! FetchedTasks
            return result
        }()
        
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.tableView?.separatorStyle = .none
        self.tableView?.indicatorStyle = .default
        
        super.init()
        
        fetchedResultController?.delegate = self
        
        // setup Table View properties
        setupTableView()
    }
    
    deinit {
        tableView?.delegate = nil
        tableView?.dataSource = nil
        fetchedResultController?.delegate = nil
    }
    
    //    MARK: - Custom function
    
    func parentDidDisappear() {
        fetchedResultController?.delegate = nil
    }
    
    func parentWillAppear() {
        fetchedResultController?.delegate = self
    }
    
    /// Изменить сортировку
    func changeSorting(By sort: [NSSortDescriptor]) {
        fetchedResultController?.fetchRequest.sortDescriptors = sort
        getFetch()
        reload()
    }
    
    /// Изменить, скрыть или показать выполненные задачи
    func toogleCompleteSort() {
        showCompleteTask.toggle()
        fetchedResultController?.fetchRequest.predicate = showCompleteTask.validate()
        getFetch()
        reload()
    }
    
    /// Перезагрузить таблицу
    func reload() {
        if tableView?.window == nil {
            tableView?.reloadData()
        } else {
            tableView?.performBatchUpdates({ tableView?.reloadSections([0], with: .fade)}, completion: nil)
        }
    }
    
    func createNewTask(withName name: String) {
        CoreDataManager.instance.addNewValue(Value: name, Entity: "Task", KeyPath: "nameString")
        CoreDataManager.instance.saveContext()
    }
    
    func createNewChild(withName name: String, index: Int) {
        guard let parent = fetchedResultController?.fetchedObjects?[index] else { return }
        
        let st = SubTasks()
        st.nameString = name
        st.parent = parent
        CoreDataManager.instance.saveContext()
    }
    
    private func getFetch() {
        fetchedResultController = CoreDataManager.instance.fetchingData(requestController: fetchedResultController as! FetchedResult) as? FetchedTasks
    }
    
    private func setupTableView() {
        tableView?.register(TVTableViewCell.self, forCellReuseIdentifier: TVTableViewCell.reuseId)
        tableView?.dataSource = self
        tableView?.delegate = self
        
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.backgroundColor = UIColor.clear
        tableView?.tableFooterView = UIView()
        
        tableView?.estimatedRowHeight = 50
        tableView?.rowHeight = UITableView.automaticDimension
    }
}

//    MARK: - TableView Delegate

extension TaskListDataProvider: UITableViewDelegate {
    
    //    MARK: - TableView Gesture Recognizer
    
    // На свайп появляется меню, в котором можно удалить ячейку или перейти на настройку принадлежащей ей задачи
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: S.Actions.Delete, handler: { [weak self] (_,_, result) in
            // Проверка существования напоминания и его удаление из системной программы Напоминания
            if
                let task = self?.fetchedResultController?.object(at: indexPath),
                let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: task.deadlineIdentifier)
            {
                ReminderManager.instance.removeReminder(userReminder: dependReminder)
            }
            
            if let controller = self?.fetchedResultController as? FetchedResult {
                CoreDataManager.instance.deleteObject(at: indexPath, from: controller)
                CoreDataManager.instance.saveContext()
            }
            
            result(true)
        })
        deleteAction.image = StcImage.deleteIcon
        deleteAction.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: S.Actions.Edit) { [weak self] (_,_, result) in
            if let task = self?.fetchedResultController?.object(at: indexPath) {
                NotificationCenter.default.post(
                    name: NSNotification.Name(rawValue: StcStr.Notif.TLV.detailAction),
                    object: nil,
                    userInfo: ["data" : task]
                )
            }
            result(true)
        }
        editAction.image =  StcImage.editIcon
        editAction.backgroundColor = UIColor(white: 1, alpha: 0.001)
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let task = self.fetchedResultController?.object(at: indexPath) {
            NotificationCenter.default.post(
                name: NSNotification.Name(rawValue: StcStr.Notif.TLV.subtaskAction),
                object: nil,
                userInfo: ["data" : task]
            )
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//    MARK: - TableView DataSource

extension TaskListDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultController?.fetchedObjects else { return 0 }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TVTableViewCell.reuseId, for: indexPath) as? TVTableViewCell, let task = fetchedResultController?.object(at: indexPath)
        else { return UITableViewCell() }
        
        if (task.deadlineIdentifier != nil) && (ReminderManager.instance.checkCalendarAccess()) {
            task.compareAchievedTaskAndReminder()
        }
        
        let subTasks = SubTasks.getSubTaskForTask(identifier: task).fetchedObjects
        cell.fillWithContent(
            taskName: task.nameString,
            taskDate: task.dateForLabel,
            isAcheved: task.isAchieved,
            subTaskList: subTasks ?? [],
            row: indexPath.row
        )
        cell.tag = indexPath.row
        
        return cell
    }
}

//    MARK: - Fetched Results Controller Delegate

extension TaskListDataProvider: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: FetchedResult) {
        tableView?.beginUpdates()
        print(" ")
        print("\(self): \(#function) Start updates on TVTask TableView")
    }
    
    func controller(_ controller: FetchedResult, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                print("\(self): \(#function) Insert Cell on \(indexPath.row) in TaskTableView")
                tableView?.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                print("\(self): \(#function) Update Cell on \(indexPath.row) in TaskTableView")
                
                if let cell = tableView?.cellForRow(at: indexPath) as? TVTableViewCell, let task = fetchedResultController?.object(at: indexPath) {
                    
                    let subTasks = SubTasks.getSubTaskForTask(identifier: task).fetchedObjects
                    cell.updateData(
                        taskName: task.nameString,
                        isAcheved: task.isAchieved,
                        subTaskList: subTasks ?? []
                    )
                }
            }
        case .move:
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView?.insertRows(at: [newIndexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                print("\(self): \(#function) Delete Cell on \(indexPath.row) in TaskTableView")
                tableView?.deleteRows(at: [indexPath], with: .automatic)
            }
        @unknown default:
            print("\(self): \(#function) Unknown default NSFetchedResultsChangeType")
        }
    }
    
    func controllerDidChangeContent(_ controller: FetchedResult) {
        tableView?.endUpdates()
        print("\(self): \(#function) End updates on TVTask TableView")
        print(" ")
    }
    
}
