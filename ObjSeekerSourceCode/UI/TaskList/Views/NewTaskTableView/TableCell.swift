//
//  NewTableCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 20/08/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class TVTableViewCell: UITableViewCell {
    
//     MARK: - Properties
    
    static let reuseId = "TVTableViewCell"
    
    private var nameStringLabel: UILabel = {
        let label = UILabel()
        label.font = Style.UserFonts.taskMainFont
        label.numberOfLines = 4
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.accessibilityIdentifier = "nameStringLabel_TVTableViewCell"
        return label
    }()
    
    private var dateStringLabel: UILabel = {
        let label = UILabel()
        label.font = Style.UserFonts.taskAddFont
        label.translatesAutoresizingMaskIntoConstraints = false
        label.accessibilityIdentifier = "dateStringLabel_TVTableViewCell"
        return label
    }()
    
    private var contentTaskView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "contentTaskView_TVTableViewCell"
        return view
    }()
    
    private var subTaskCollectionView: TaskCollectionView
    
//     MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        subTaskCollectionView = TaskCollectionView()
        subTaskCollectionView.accessibilityIdentifier = "subTaskCollectionView_TVTableViewCell"
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        backgroundColor = .clear
        contentView.backgroundColor = UIColor.clear
        contentTaskView.backgroundColor = Style.UserColors.cellUndoneColor
        contentTaskView.layer.cornerRadius = 10
        setsContentWithTime()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

//     MARK: - custom functions
    
    func fillWithContent(
        taskName: String,
        taskDate: String?,
        isAcheved: Bool,
        subTaskList: [SubTasks],
        collectionVL: UICollectionViewLayout = UICollectionViewLayout(),
        row: Int
    ) {
        nameStringLabel.text = taskName

        if let taskDate = taskDate {
            dateStringLabel.text = taskDate
            dateStringLabel.isHidden = false
        } else {
            dateStringLabel.isHidden = true
        }

        coloring(with: isAcheved)
        
        subTaskCollectionView.fillCollection(with: subTaskList)
        subTaskCollectionView.tag = row
    }
    
    func updateData(
        taskName: String,
        isAcheved: Bool,
        subTaskList: [SubTasks]
    ) {
        nameStringLabel.text = taskName
        coloring(with: isAcheved)
        
        subTaskCollectionView.fillCollection(with: subTaskList)
    }
    
    private func coloring(with value: Bool) {
        if value {
            nameStringLabel.taskIsComplete()
            contentTaskView.backgroundColor = Style.UserColors.cellDoneColor
        } else {
            nameStringLabel.taskIsUncomplete()
            contentTaskView.backgroundColor = Style.UserColors.cellUndoneColor
        }
    }
    
//     MARK: - Layout for cell
    private func setsContentWithTime() {
//         setting views on Host View
        contentView.addSubview(contentTaskView)
        contentView.addSubview(nameStringLabel)
        contentView.addSubview(dateStringLabel)
        contentView.addSubview(subTaskCollectionView)
        
//         contentTaskView
        contentTaskView.topAnchor
            .constraint(equalTo: contentView.topAnchor, constant: 6)
            .isActive = true
        contentTaskView.leftAnchor
            .constraint(equalTo: contentView.leftAnchor, constant: 8)
            .isActive = true
        contentTaskView.rightAnchor
            .constraint(equalTo: contentView.rightAnchor, constant: -8)
            .isActive = true
        contentTaskView.bottomAnchor
            .constraint(equalTo: dateStringLabel.bottomAnchor, constant: 36)
            .isActive = true
        contentTaskView.centerXAnchor
            .anchorWithOffset(to: contentView.centerXAnchor)
        
//         nameStringLabel
        nameStringLabel.topAnchor
            .constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 12)
            .isActive = true
        nameStringLabel.leadingAnchor
            .constraint(equalTo: contentView.safeAreaLayoutGuide.leadingAnchor, constant: 12)
            .isActive = true
        nameStringLabel.trailingAnchor
            .constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor, constant: -12)
            .isActive = true
        nameStringLabel.centerXAnchor
            .anchorWithOffset(to: contentView.safeAreaLayoutGuide.centerXAnchor)
        
//         dateStringLabel
        dateStringLabel.topAnchor
            .constraint(equalTo: nameStringLabel.bottomAnchor, constant: 4)
            .isActive = true
        dateStringLabel.centerXAnchor
            .anchorWithOffset(to: nameStringLabel.centerXAnchor)
        dateStringLabel.leadingAnchor
            .constraint(equalTo: nameStringLabel.leadingAnchor, constant: 10)
            .isActive = true
        dateStringLabel.trailingAnchor
            .constraint(equalTo: nameStringLabel.trailingAnchor, constant: -10)
            .isActive = true
        
        // subTaskCollectionView
        subTaskCollectionView.topAnchor
            .constraint(equalTo: dateStringLabel.bottomAnchor, constant: 8)
            .isActive = true
        subTaskCollectionView.trailingAnchor
            .constraint(equalTo: contentView.trailingAnchor, constant: 0)
            .isActive = true
        subTaskCollectionView.leadingAnchor
            .constraint(equalTo: contentView.leadingAnchor, constant: 0)
            .isActive = true
        subTaskCollectionView.bottomAnchor
            .constraint(equalTo: contentView.bottomAnchor, constant: 0)
            .isActive = true
        subTaskCollectionView.heightAnchor
            .constraint(equalToConstant: 70)
            .isActive = true
    }
}
