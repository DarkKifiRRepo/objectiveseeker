//
//  CustomSubTaskCollectionViewCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 20/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//

import UIKit

// TODO: здесь есть утечка - найти вылечить
class CustomSubTaskCollectionViewCell: UICollectionViewCell {
    
    //     MARK: - Properties
    static let reuseId = "subTaskCell"
    private var subTaskLabel: UILabel?
    
    //    MARK: - View Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setCellApperiance()
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //     MARK: - Layout for cell
    
    func fill(with subtask: SubTasks) {
        subTaskLabel?.text = subtask.nameString
        if subtask.isAchieved {
            contentView.backgroundColor = Style.UserColors.cellDoneColor
            subTaskLabel?.taskIsComplete()
        } else {
            contentView.backgroundColor = Style.UserColors.cellUndoneColor
            subTaskLabel?.taskIsUncomplete()
        }
    }
    
    private func setCellApperiance() {
        subTaskLabel = generateLabel()
        contentView.addSubview(subTaskLabel!)
        subTaskLabel?.fillSuperview()

        contentView.cornerRadius = 12
        contentView.borderColor = Style.UserColors.borderForCells
        contentView.borderWidth = 2
    }
    
    private func generateLabel() -> UILabel {
        let label = UILabel()
        label.text = ""
        label.font = Style.UserFonts.collectionFont
        label.textAlignment = .center
        label.numberOfLines = 3
        label.translatesAutoresizingMaskIntoConstraints = false
        label.accessibilityIdentifier = "subTaskLabel_CustomSubTaskCollectionViewCell"
        return label
    }
}
