//
//  CollectionViewFooterCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 16/01/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CollectionViewFooterCell: UICollectionReusableView {
    //     MARK: - Properties
    
    static let reuseId = "newSubTaskAddFooter"
    
    var button: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //     MARK: - Layout for cell
    func setupLayout() {
        self.backgroundColor = Style.UserColors.cellDoneColor
        self.borderColor = Style.UserColors.borderForCells
        self.borderWidth = 2
        self.cornerRadius = 12
        
        button = generateButton()
        if let button = button {
            addSubview(button)
            button.fillSuperview()
        }
    }
    
    private func generateButton() -> UIButton {
        let button = UIButton()
        let attrib = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 48, weight: .regular),
            NSAttributedString.Key.foregroundColor: Style.UserColors.textComplete,
        ]
        let title = NSAttributedString(string: "✚", attributes: attrib)
        button.setAttributedTitle(title, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.accessibilityIdentifier = "button_CollectionViewFooterCell"
        return button
    }
}
