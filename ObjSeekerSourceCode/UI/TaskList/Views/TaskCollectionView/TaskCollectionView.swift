//
//  TaskCollectionView.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 29/05/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class TaskCollectionView: UICollectionView {
    
    //     MARK: - Properties
    
    private var showedData: [SubTasks] = []
    
    private var flowLayout: UICollectionViewFlowLayout?
    
    //    MARK: - View Lifecycle
    
    init() {
        flowLayout = UICollectionViewFlowLayout()
        flowLayout?.scrollDirection = .horizontal
        flowLayout?.itemSize = CGSize(width: 130, height: 70)
        flowLayout?.footerReferenceSize = CGSize(width: 70, height: 70)
        
        super.init(frame: .zero, collectionViewLayout: flowLayout!)
        
        showsHorizontalScrollIndicator = false
        translatesAutoresizingMaskIntoConstraints = false
        readyClass()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //    MARK: - Custom Functions
    
    private func readyClass() {
        delegate = self
        dataSource = self
        backgroundColor = .clear
        
        register(
            CustomSubTaskCollectionViewCell.self,
            forCellWithReuseIdentifier: CustomSubTaskCollectionViewCell.reuseId
        )
        register(
            CollectionViewFooterCell.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: CollectionViewFooterCell.reuseId
        )
    }
    
    deinit {
        delegate = nil
        dataSource = nil
        showedData = []
        flowLayout = nil
    }
    
    func fillCollection(with data: [SubTasks]) {
        showedData = data
        reloadData()
    }
    
    func clearCollection() {
        showedData = []
    }
}

//    MARK: - CollectionView Delegate

extension TaskCollectionView: UICollectionViewDelegate {}

//    MARK: - CollectionView DataSource

extension TaskCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CustomSubTaskCollectionViewCell.reuseId,
            for: indexPath
        ) as? CustomSubTaskCollectionViewCell {
            let subTask = showedData[indexPath.row]
            cell.fill(with: subTask)
            return cell
        }
        return UICollectionViewCell()
    }
}

//    MARK: - CollectionView DelegateFlowLayout

extension TaskCollectionView: UICollectionViewDelegateFlowLayout {
    // Добавляем Footer для collectionView с помощью которого можно будет добавить новую подзадачу прямо с главного экрана
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerCell = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: CollectionViewFooterCell.reuseId,
            for: indexPath
        ) as! CollectionViewFooterCell
        
        footerCell.button?.addTarget(self, action: #selector(footerTapped), for: .touchUpInside)

        return footerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 10)
    }
    
    @objc func footerTapped() {
        let tag = self.tag
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: StcStr.Notif.TLV.addAction),
            object: nil,
            userInfo: ["data": tag]
        )
    }
}
