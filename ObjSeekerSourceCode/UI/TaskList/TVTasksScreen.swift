//
//  NewTVTasks.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 24/08/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class NewTVTasks: UIViewController {
    
    //     MARK: - Properties
    
    private var sortingData: SortingData?
    private let dataProvider: TaskListDataProvider?
    private var router: Navigation?
    
    //     MARK: - View Lifecycle
    
    init(dataProvider: TaskListDataProvider?, navigation: Navigation) {
        
        self.dataProvider = dataProvider
        self.router = navigation
        sortingData = SortingData(array: nil)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: StcStr.Notif.TLV.detailAction),
            object: nil,
            queue: .main,
            using: { [weak self] notification in
                self?.detailRequest(notification: notification)
        })
        
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: StcStr.Notif.TLV.subtaskAction),
            object: nil,
            queue: .main,
            using: { [weak self] notification in
                self?.cellTapped(notification: notification)
        })
        
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: StcStr.Notif.TLV.addAction),
            object: nil,
            queue: .main,
            using: { [weak self] notification in
                self?.footerPlusTapped(notification: notification)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataProvider?.parentWillAppear()
        dataProvider?.reload()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dataProvider?.tableView?.frame = view.bounds
        guard let gradientLayer = view.layer.sublayers?[0] as? CAGradientLayer else { return }
        gradientLayer.frame = view.bounds
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        dataProvider?.parentDidDisappear()
        NotificationCenter.default.removeObserver(self)
    }
    
    //    MARK: - Custom function
    
    func setupViews() {
        if let tableView = dataProvider?.tableView {
            tableView.fillSuperview()
            view.addSubview(tableView)
        }
        
        let gradient = LayerForViews.gradiented(
            toView: view,
            colors:
                [
                    Style.UserColors.bgTop.cgColor,
                    Style.UserColors.bgBottom.cgColor,
                ]
        )
        view.layer.insertSublayer(gradient, at: 0)
        
        setupNavBar()
    }
    
    func setupNavBar() {
        let addTaskButton = UIBarButtonItem(
            image: StcImage.addIcon,
            landscapeImagePhone: StcImage.addIcon,
            style: .plain,
            target: self,
            action: #selector(rightBarButtonPressed)
        )
        let sortButton = UIBarButtonItem(
            image: StcImage.sortIcon,
            landscapeImagePhone: StcImage.sortIcon,
            style: .plain,
            target: self,
            action: #selector(sortButtonTapped)
        )
        
        navigationItem.rightBarButtonItem = addTaskButton
        navigationItem.leftBarButtonItem = sortButton
        navigationItem.title = S.NavBar.TaskList
        navigationItem.largeTitleDisplayMode = .always
    }

    //         MARK: - Actions
    
    @objc func rightBarButtonPressed() {
        AlertTextFieldController.createAndShow(
            parent: self,
            alertType: .alert,
            placeholder: .tvTask,
            title: S.Alert.NewTask,
            actionHandler:  { [weak self] text in
                guard let unwrappedText = text, unwrappedText != "" else { return }
                self?.dataProvider?.createNewTask(withName: unwrappedText)
        })
    }
    
    @objc func sortButtonTapped() {
        if let sortArray = sortingData {
            AlertSortController.createAndShow(
                parent: self,
                alertType: .sheet,
                placeholder: .tvTask,
                title: S.Sheet.SortBy,
                sortingArray: sortArray.descriptors,
                sortBy: sortArray.currentState,
                actionHandler: { [weak self] (newState) in
                    guard let newState = newState else { return }
                    let newSort = SortingData(state: newState, array: sortArray.descriptors)
                    self?.sortingData = newSort
                    self?.dataProvider?.changeSorting(By: newSort.descriptors)
                }
            )
        }
    }
}

//        MARK: - Notification Actions

extension NewTVTasks {
    private func detailRequest(notification: Notification) {
        guard let userInfo = notification.userInfo, let task = userInfo["data"] as? Task else { return }
        router?.pushDetailScreen(task: task)
    }
    
    private func cellTapped(notification: Notification) {
        guard let userInfo = notification.userInfo, let task = userInfo["data"] as? Task else { return }
        router?.pushSubTaskScreen(subTask: task)
    }
    
    private func footerPlusTapped(notification: Notification) {
        guard let userInfo = notification.userInfo, let taskIndex = userInfo["data"] as? Int else { return }
        AlertTextFieldController.createAndShow(parent: self, alertType: .alert, placeholder: .tvTask, title: S.Alert.NewTask, actionHandler:  { [weak self] text in
            guard let unwrappedText = text, unwrappedText != "" else { return }
            self?.dataProvider?.createNewChild(withName: unwrappedText, index: taskIndex)
        })
    }
}
