//
//  TimeSetterCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 21/09/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class TimeSetterCell: UITableViewCell {
//    MARK: - Properties
    
    static let reuseID = DetailDataProvider.ReuseIndentifier.TimeCell.rawValue
    
    private var updateEditingTask: ((DetailDataProvider.DataTransferObject.ReminderState) -> ()) = { _ in }
    private var taskDate: Date? {
        didSet {
            if let date = taskDate {
                let dateString = dateFormatter.string(from: date)
                dateLabel.text = dateString
            }
        }
    }
    
    private let dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.dateFormat = "dd.MM.yy 'г., ' HH:mm"
        return dt
    }()
    
    /// Label "Добавить напоминание"
    private let addNotificationLabel: UILabel = {
        let lb = UILabel()
        lb.text = S.DView.NotificationAdd
        lb.font = Style.UserFonts.detailViewFont
        lb.textColor = .black
        lb.textAlignment = NSTextAlignment.left
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.accessibilityIdentifier = "addNotificationLabel_TimeSetterCell"
        return lb
    }()
    
    private let switcher: UISwitch = {
        let sw = UISwitch()
        sw.setOn(false, animated: false)
        sw.translatesAutoresizingMaskIntoConstraints = false
        sw.accessibilityIdentifier = "switcher_TimeSetterCell"
        return sw
    }()
    
    /// Label "Время напоминания"
    private let descriptionDateLabel: UILabel = {
        let lb = UILabel()
        lb.font = Style.UserFonts.detailViewFont
        lb.text = S.DView.NotificationTime
        lb.textColor = Style.UserColors.textColor
        lb.textAlignment = NSTextAlignment.left
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.accessibilityIdentifier = "descriptionDateLabel_TimeSetterCell"
        return lb
    }()
    
    /// Label отражающий дату напоминания
    private let dateLabel: UILabel = {
        let lb = UILabel()
        lb.font = Style.UserFonts.detailViewFont
        lb.text = S.Other.EmptyString
        lb.textColor = Style.UserColors.textColor
        lb.textAlignment = NSTextAlignment.right
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.accessibilityIdentifier = "dateLabel_TimeSetterCell"
        return lb
    }()
    
    private var contentTaskView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.cornerRadius = 8
        view.borderWidth = 1.5
        view.borderColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "contentTaskView_TimeSetterCell"
        return view
    }()
    
    private var datePicker: UIDatePicker = {
        let dt = UIDatePicker()
        dt.setDate(Date(), animated: false)
        dt.setValue(UIColor.black, forKey: "textColor")
        dt.backgroundColor = .white
        if #available(iOS 14, *) {
            dt.preferredDatePickerStyle = .wheels
        }
        dt.translatesAutoresizingMaskIntoConstraints = false
        dt.accessibilityIdentifier = "datePicker_TimeSetterCell"
        return dt
    }()
    
//    MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        switcher.addTarget(
            self,
            action: #selector(switcherPressed),
            for: .touchUpInside
        )
        datePicker.addTarget(
            self,
            action: #selector(datePickerChangeValue),
            for: .valueChanged
        )
        setupDefaultViews(isStateClosed: true)
    }
    
    convenience init(
        state: DetailDataProvider.State?,
        taskDateString: String?,
        dateToShow: Date?,
        updateEditingTask: @escaping ((DetailDataProvider.DataTransferObject.ReminderState) -> ())
    ) {
        self.init()
        
        self.updateEditingTask = updateEditingTask
        self.datePicker.date = dateToShow ?? Date()
        dateLabel.text = taskDateString ?? dateFormatter.string(from: Date())
        state == .closed ? switcher.setOn(false, animated: false) : switcher.setOn(true, animated: false)

        if let state = state {
            setupState(state)
        } else {
            setupState(.closed)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    MARK: Custom Function
    
    @objc func switcherPressed() {
        let state = switcher.isOn ? DetailDataProvider.State.timeShowed : DetailDataProvider.State.closed
        switch (state, taskDate) {
        case (.closed, _):
            taskDate = nil
            updateEditingTask(.notSet)
        case (.timeShowed, .none):
            taskDate = Date()
            updateEditingTask(.set(taskDate))
        case (.timeShowed, .some), (.pickerShowed, _):
            break
        }
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: StcStr.Notif.DV.addReminder),
            object: nil,
            userInfo: ["data" : state]
        )
    }
    
    @objc func handleGesture(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            NotificationCenter.default.post(
                name: NSNotification.Name(rawValue: StcStr.Notif.DV.label),
                object: nil,
                userInfo: ["data" : DetailDataProvider.State.pickerShowed]
            )
        }
    }
    
    @objc func datePickerChangeValue() {
        let date = datePicker.date
        taskDate = date
        updateEditingTask(.set(date))
    }
    
    private func setGestureRecognizer(with view: UILabel) {
        let gr = UITapGestureRecognizer()
        gr.addTarget(self, action: #selector(handleGesture))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gr)
    }
    
    private func setupState(_ state: DetailDataProvider.State) {
        switch state {
        case .closed:
            setupDefaultViews(isStateClosed: true)
        case .timeShowed:
            setupDefaultViews(isStateClosed: false)
            setupDateLabel(inMiddle: false)
        case .pickerShowed:
            setupDefaultViews(isStateClosed: false)
            setupDateLabel(inMiddle: true)
            setupDatePicker()
        }
    }
    
//     MARK: - Layout for cell
    
    private func setupDefaultViews(isStateClosed state: Bool) {
        backgroundColor = .clear
        contentView.addSubview(contentTaskView)
        setupNotificationWithSwitcher(isOne: state)
        
        contentTaskView.anchor(
            top: contentView.topAnchor,
            leading: contentView.leadingAnchor,
            bottom: contentView.bottomAnchor,
            trailing: contentView.trailingAnchor,
            padding: UIEdgeInsets(horizontal: 4, vertical: 2)
        )
    }
    
    private func setupNotificationWithSwitcher(isOne: Bool) {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.addArrangedSubview(addNotificationLabel)
        stack.addArrangedSubview(switcher)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.accessibilityIdentifier = "stack_TimeSetterCell"
        contentView.addSubview(stack)
        
        stack.anchor(
            top: contentTaskView.topAnchor,
            leading: contentTaskView.leadingAnchor,
            bottom: isOne ? contentTaskView.bottomAnchor : nil,
            trailing: contentTaskView.trailingAnchor,
            padding: UIEdgeInsets(all: 12)
        )
    }
    
    private func setupDateLabel(inMiddle middle: Bool) {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.addArrangedSubview(descriptionDateLabel)
        stack.addArrangedSubview(dateLabel)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.accessibilityIdentifier = "stack_TimeSetterCell"
        contentView.addSubview(stack)
        
        setGestureRecognizer(with: descriptionDateLabel)
        setGestureRecognizer(with: dateLabel)
        
        stack.anchor(
            top: addNotificationLabel.bottomAnchor,
            leading: contentTaskView.leadingAnchor,
            bottom: middle ? nil : contentTaskView.bottomAnchor,
            trailing: contentTaskView.trailingAnchor,
            padding: UIEdgeInsets(all: 12)
        )
    }
    
    private func setupDatePicker() {
        contentView.addSubview(datePicker)
        
        datePicker.anchor(
            top: descriptionDateLabel.bottomAnchor,
            leading: contentTaskView.leadingAnchor,
            bottom: contentTaskView.bottomAnchor,
            trailing: contentTaskView.trailingAnchor,
            padding: UIEdgeInsets(all: 12)
        )
    }
}
