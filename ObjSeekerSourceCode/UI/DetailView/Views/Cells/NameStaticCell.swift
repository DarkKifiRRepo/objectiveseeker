//
//  NameStaticCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 21/09/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class NameStaticCell: UITableViewCell {
    
//     MARK: - Properties
    
    static let reuseID = DetailDataProvider.ReuseIndentifier.NameCell.rawValue
    private var taskName: String = "Error in Initialze task name property!"
    private var updateTextViewSize: (() -> ()) = {}
    private var updateEditingTask: ((String) -> ()) = { _ in }

    private let nameTextField: UITextView = {
        let tf = UITextView()
        tf.font = Style.UserFonts.detailViewFont
        tf.textColor = .black
        tf.backgroundColor = .white
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.autocapitalizationType = .words
        tf.autocorrectionType = .no
        tf.isScrollEnabled = false
        tf.accessibilityIdentifier = "nameTextField_NameStaticCell"
        return tf
    }()
    
    private var contentTaskView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.cornerRadius = 8
        view.borderWidth = 1.5
        view.borderColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "contentTaskView_NameStaticCell"
        return view
    }()
    
//     MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        nameTextField.delegate = self
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillWithContent(
        _ taskName: String,
        updateTextViewSize: @escaping (() -> ()),
        updateEditingTask: @escaping ((String) -> ())
    ) {
        self.updateTextViewSize = updateTextViewSize
        self.updateEditingTask = updateEditingTask
        nameTextField.text = taskName
    }
    
//     MARK: - Layout for cell
    
    private func setupViews() {
        backgroundColor = .clear
        contentView.addSubview(contentTaskView)
        contentView.addSubview(nameTextField)
        nameTextField.fillSuperview(
            padding: UIEdgeInsets(horizontal: 12, vertical: 24)
        )
        
        contentTaskView.anchor(
            top: contentView.topAnchor,
            leading: contentView.leadingAnchor,
            bottom: contentView.bottomAnchor,
            trailing: contentView.trailingAnchor,
            padding: UIEdgeInsets(top: 4, left: 4, bottom: 2, right: 4)
        )
    }
    
//    MARK: Custom Function
    
    private func setDefaultName() {
        nameTextField.text = taskName
    }
}

// MARK: - TextView management

extension NameStaticCell: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        nameTextField.resignFirstResponder()
        if textView.text.isEmpty {
            setDefaultName()
        } else {
            guard let newName = nameTextField.text, taskName != newName else { return }
            taskName = newName
            updateEditingTask(newName)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            nameTextField.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let size = nameTextField.bounds.size
        let newSize = nameTextField.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        if size.height != newSize.height {
            updateTextViewSize()
        }
        updateEditingTask(nameTextField.text)
    }
}
