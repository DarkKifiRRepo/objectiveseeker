//
//  DoneButtonCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 27.02.2020.
//  Copyright © 2020 com.Evseev. All rights reserved.
//

import UIKit

class DoneButtonCell: UITableViewCell {
    
//     MARK: - Properties
    
    static let reuseID = DetailDataProvider.ReuseIndentifier.DoneButtonCell.rawValue
    private let button: UIButton = {
        let button = UIButton()
        let attrib = [
            NSAttributedString.Key.font: Style.UserFonts.taskMainFont,
            NSAttributedString.Key.foregroundColor: Style.UserColors.textComplete,
        ]
        let title = NSAttributedString(string: S.DView.SaveButton, attributes: attrib)
        button.setAttributedTitle(title, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.accessibilityIdentifier = "button_DoneButtonCell"
        return button
    }()
    
//     MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(isReady: Bool) {
        self.init()
        
        setupViews(isReady)
        
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    @objc func buttonPressed() {
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: StcStr.Notif.DV.saveButton),
            object: nil
        )
    }
    
    private func setupViews(_ isReady: Bool) {
        self.backgroundColor = .clear
        
        button.cornerRadius = 8.0
        button.borderWidth = 1.5
        button.titleEdgeInsets = UIEdgeInsets(all: 12.0)
        button.isEnabled = isReady
        button.backgroundColor = isReady ? Style.UserColors.cellDoneColor : .gray
        button.borderColor = isReady ? Style.UserColors.borderForCells : .darkGray
        
        contentView.addSubview(button)
        
        button.fillSuperview(padding: UIEdgeInsets(all: 8.0))
    }
}
