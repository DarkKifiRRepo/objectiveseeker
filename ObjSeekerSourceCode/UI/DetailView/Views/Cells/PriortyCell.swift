//
//  NameStaticCell.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 21/09/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class PriorityCell: UITableViewCell {
    
//     MARK: - Properties
    
    static let reuseID = DetailDataProvider.ReuseIndentifier.PriorityCell.rawValue
    private var updateEditingTask: ((Float) -> ()) = { _ in }
    
    private let prioritySlider: UISlider = {
        let ps = UISlider()
        ps.minimumValue = 0
        ps.maximumValue = 10
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.accessibilityIdentifier = "prioritySlider_PriorityCell"
        return ps
    }()
    
    private let minLabel: UILabel = {
        let lb = UILabel()
        lb.text = S.DView.min
        lb.font = Style.UserFonts.detailViewFont
        lb.textColor = .black
        lb.textAlignment = NSTextAlignment.center
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.accessibilityIdentifier = "minLabel_PriorityCell"
        return lb
    }()
    
    private let maxLabel: UILabel = {
        let lb = UILabel()
        lb.text = S.DView.max
        lb.font = Style.UserFonts.detailViewFont
        lb.textColor = .black
        lb.textAlignment = NSTextAlignment.center
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.accessibilityIdentifier = "maxLabel_PriorityCell"
        return lb
    }()
    
    private var contentTaskView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.cornerRadius = 8
        view.borderWidth = 1.5
        view.borderColor = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.accessibilityIdentifier = "contentTaskView_PriorityCell"
        return view
    }()
    
//     MARK: - view lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillWithContent(_ task: TaskProtocol, updatePrioriry: @escaping ((Float) -> ())) {
        self.updateEditingTask = updatePrioriry
        prioritySlider.setValue(Float(task.payload.priority), animated: false)
        prioritySlider.addTarget(self, action: #selector(sliderChangeValue), for: .valueChanged)
    }
    
    @objc func sliderChangeValue() {
        updateEditingTask(prioritySlider.value)
    }
    
//     MARK: - Layout for cell
    
    private func setupViews() {
        backgroundColor = .clear
        contentView.addSubview(contentTaskView)
        contentView.addSubview(prioritySlider)
        contentView.addSubview(minLabel)
        contentView.addSubview(maxLabel)
        
        minLabel.anchor(
            top: contentTaskView.topAnchor,
            leading: contentTaskView.leadingAnchor,
            bottom: contentTaskView.bottomAnchor,
            trailing: nil,
            padding: UIEdgeInsets(all: 12)
        )
        
        maxLabel.anchor(
            top: contentTaskView.topAnchor,
            leading: nil,
            bottom: contentTaskView.bottomAnchor,
            trailing: contentTaskView.trailingAnchor,
            padding: UIEdgeInsets(all: 12)
        )
        
        prioritySlider.anchor(
            top: contentTaskView.topAnchor,
            leading: minLabel.trailingAnchor,
            bottom: contentTaskView.bottomAnchor,
            trailing: maxLabel.leadingAnchor,
            padding: UIEdgeInsets(horizontal: 8, vertical: 12)
        )

        contentTaskView.anchor(
            top: contentView.topAnchor,
            leading: contentView.leadingAnchor,
            bottom: contentView.bottomAnchor,
            trailing: contentView.trailingAnchor,
            padding: UIEdgeInsets(horizontal: 4, vertical: 2)
        )
    }
}
