//
//  TVDetailDataProvider.swift
//  ObjSeeker
//
//  Created by funbox on 12/02/2020.
//  Copyright © 2020 com.Evseev. All rights reserved.
//

import UIKit

class DetailDataProvider: NSObject {
    let tableView: UITableView
    private let data: DataTransferObject
    private let reminderManager: ReminderManager?
    private var lastSaveButtonState = false
    
    var notificationState: State? {
        didSet {
            tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        }
    }
    
    init(task: TaskProtocol? = nil, reminder: ReminderManager? = nil) {
        
        self.tableView = UITableView(frame: CGRect.zero, style: .plain)
        self.tableView.separatorStyle = .none
        self.tableView.indicatorStyle = .default
        
        self.reminderManager = reminder
        
        data = .init(initialValue: task)
        
        notificationState = data.task?.payload.deadlineIdentifier != nil
            ? .timeShowed
            : .closed
        
        super.init()
        setupTableView()
        tableView.reloadData()
    }
    
    private func setupTableView() {
        tableView.register(NameStaticCell.self, forCellReuseIdentifier: NameStaticCell.reuseID)
        tableView.register(TimeSetterCell.self, forCellReuseIdentifier: TimeSetterCell.reuseID)
        tableView.register(PriorityCell.self, forCellReuseIdentifier: PriorityCell.reuseID)
        tableView.register(DoneButtonCell.self, forCellReuseIdentifier: DoneButtonCell.reuseID)
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.clear
        tableView.tableFooterView = UIView()
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    func checkCalendarAccess() {
        reminderManager?.setCalendarAccess()
    }
    
    func set(state: State) {
        if notificationState != state {
            notificationState = state
        }
    }
}

extension DetailDataProvider: UITableViewDelegate {}

extension DetailDataProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let task = data.task else { return UITableViewCell() }
        switch indexPath.row {
        case 0:
            let cell = NameStaticCell()
            cell.fillWithContent(
                task.payload.nameString,
                updateTextViewSize: { [unowned self] in
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                },
                updateEditingTask: { [unowned self] taskName in
                    self.data.changes.name = taskName
                    self.checkChangesForSave()
                })
            return cell
        case 1:
            let cell = TimeSetterCell(
                state: notificationState,
                taskDateString: task.payload.date,
                dateToShow: task.payload.deadlineDate,
                updateEditingTask: { [unowned self] taskDate in
                    self.data.changes.reminder = taskDate
                    self.checkChangesForSave()
                })
            return cell
        case 2:
            let cell = PriorityCell()
            cell.fillWithContent(
                task,
                updatePrioriry: { [unowned self] taskPriority in
                    self.data.changes.priority = Int(taskPriority)
                    self.checkChangesForSave()
                })
            return cell
        case 3:
            let cell = DoneButtonCell(isReady: data.isHaveChanges)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension DetailDataProvider {
    private func checkChangesForSave() {
        let temproraryState = data.isHaveChanges
        if temproraryState != lastSaveButtonState {
            lastSaveButtonState = temproraryState
            tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
        }
    }
    
    func trySaveNameAndPriority() {
        data.saveData()
    }
    
    func trySaveNotificationChanges() {        
        guard reminderManager?.checkCalendarAccess() ?? false else { return }
        
        switch (data.initial.reminder, data.changes.reminder) {
        case (let .set(initDate), let .set(newDate)) where initDate != newDate:
            updateNotification(task: data.task, taskDueDate: newDate, manager: reminderManager)
        case (.set(_), .set(_)):
            break
        case (.set(_), .none), (.set(_), .notSet):
            removeNotification(task: data.task, manager: reminderManager)
        case (.none, let .set(newDate)), (.notSet, let .set(newDate)):
            addNotification(task: data.task, taskDueDate: newDate, manager: reminderManager)
        case (.none, .notSet), (.notSet, .notSet), (.notSet, .none), (.none, .none):
            break
        }
    }
    
    // TODO: Убедиться в правильности
    private func addNotification(task: TaskProtocol?, taskDueDate: Date?, manager: ReminderManager?) {
        if let reminderTitle = task?.payload.nameString,
           let isCompletedTask = task?.payload.isAchieved,
           let priority = task?.payload.priority,
           let taskDueDate = taskDueDate,
           let manager = manager,
           manager.checkCalendarAccess()
        {
            let taskDueTime = manager.generateDateComponents(taskDate: taskDueDate)
            let userReminder = manager.createReminder(
                title: reminderTitle,
                isCompleted: isCompletedTask,
                dueDateComponents: taskDueTime,
                dueDate: taskDueDate,
                completionDate: nil,
                priority: Int(priority)
            )
            if let identifier = manager.addReminder(userReminder: userReminder) {
                task?.payload.setDeadlineIdentifier(identifier)
            }
        }
    }
    
    // TODO: Убедиться в правильности
    private func updateNotification(task: TaskProtocol?, taskDueDate: Date?, manager: ReminderManager?) {
        let dependReminder = manager?.getCalendarItem(reminderIdentifier: data.task?.payload.deadlineIdentifier)
        if let title = task?.payload.nameString,
           let taskDueDate = taskDueDate,
           manager?.checkCalendarAccess() ?? false
        {
            let taskDueTime = manager?.generateDateComponents(taskDate: taskDueDate)
            dependReminder?.title = title
            dependReminder?.dueDateComponents = taskDueTime
        }
    }
    
    // TODO: Убедиться в правильности
    private func removeNotification(task: TaskProtocol?, manager: ReminderManager?) {
        if let dependReminder = manager?.getCalendarItem(reminderIdentifier: task?.payload.deadlineIdentifier) {
            manager?.removeReminder(userReminder: dependReminder)
            task?.payload.setDeadlineIdentifier(nil)
        }
    }
}
