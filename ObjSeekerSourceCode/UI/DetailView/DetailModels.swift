//
//  DetailModels.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 04.03.2020.
//  Copyright © 2020 com.Evseev. All rights reserved.
//

import Foundation

extension DetailDataProvider {
    enum State {
        case closed
        case timeShowed
        case pickerShowed
    }
    
    enum ReuseIndentifier: String {
        case NameCell = "NameStaticCell"
        case TimeCell = "TimeSetterCell"
        case PriorityCell = "PriorityCell"
        case DoneButtonCell = "DoneButtonCell"
    }
    
    class DataTransferObject {
        enum ReminderState: Equatable {
            case set(Date?)
            case notSet
        }

        struct State: Equatable {
            var name: String?
            var priority: Int?
            var reminder: ReminderState?
            
            init(task: TaskProtocol?) {
                name = task?.payload.nameString
                
                if let priority = task?.payload.priority {
                    self.priority = Int(priority)
                } else {
                    priority = nil
                }
                
                if let date = task?.payload.deadlineDate {
                    reminder = .set(date)
                } else {
                    reminder = .notSet
                }
            }
            
            init() {
                name = nil
                priority = nil
                reminder = nil
            }
        }
        
        let task: TaskProtocol?
        let initial: State
        var changes: State
        
        init(initialValue: TaskProtocol?) {
            task = initialValue
            initial = State(task: initialValue)
            changes = State()
        }
        
        var isHaveChanges: Bool {
            var nameChanged = false
            var priorityChanged = false
            var reminderDateChanged = false
            
            if self.changes.name != nil {
                nameChanged = self.initial.name != self.changes.name
            }
            
            if self.changes.priority != nil {
                priorityChanged = self.initial.priority != self.changes.priority
            }
            
            if self.changes.reminder != nil {
                reminderDateChanged = self.initial.reminder != self.changes.reminder
            }
            
            return nameChanged || priorityChanged || reminderDateChanged
        }
        
        func saveData() {
            if changes.name != nil {
                task?.payload.setName(changes.name!)
            }
            
            if changes.priority != nil {
                task?.payload.setPriority(Float(changes.priority!))
            }
        }
    }
}
