//
//  TVDetailTask.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 21/01/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

// Это меню отвечает за внесение сроков в задачу, переименование и установку приоритетов
// При перехода в эту форму, передается класс задача, из которой извлекается:
// - дата дэдлайна
// - приоритет
// - имя задачи
// При выходе с формы, если выставлен параметр "уведомление", то дэдлайн сохраняется в задаче
// Приоритет сохраняется при выходе с формы

class TVDetailTask: UIViewController {
    
    let dataProvider: DetailDataProvider?
    private let router: Navigation?
    
    // MARK: - View Lifecycle
    
    init(provider: DetailDataProvider? = nil, router: Navigation) {
        self.dataProvider = provider
        self.router = router
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(switcherChangedState(notification:)),
            name: NSNotification.Name(rawValue: StcStr.Notif.DV.addReminder),
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(labelChangedState(notification:)),
            name: NSNotification.Name(rawValue: StcStr.Notif.DV.label),
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(saveButtonPressed(notification:)),
            name: NSNotification.Name(rawValue: StcStr.Notif.DV.saveButton),
            object: nil
        )
        
        dataProvider?.checkCalendarAccess()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dataProvider?.tableView.frame = view.bounds
        guard let gradientLayer = view.layer.sublayers?[0] as? CAGradientLayer else { return }
        gradientLayer.frame = view.bounds
    }
    
    func setupViews() {
        if let tableView = dataProvider?.tableView {
            tableView.fillSuperview()
            view.addSubview(tableView)
        }
        
        let gradient = LayerForViews.gradiented(
            toView: view,
            colors:
            [
                Style.UserColors.bgTop.cgColor,
                Style.UserColors.bgBottom.cgColor,
            ]
        )
        view.layer.insertSublayer(gradient, at: 0)
    }
}

//        MARK: - Notification Actions

extension TVDetailTask {
    @objc func switcherChangedState(notification: Notification) {
        guard let userInfo = notification.userInfo, let state = userInfo["data"] as? DetailDataProvider.State else { return }
        dataProvider?.set(state: state)
    }
    
    @objc func labelChangedState(notification: Notification) {
        guard let userInfo = notification.userInfo, let state = userInfo["data"] as? DetailDataProvider.State else { return }
        dataProvider?.set(state: state)
    }
    
    @objc func saveButtonPressed(notification: Notification) {
        dataProvider?.trySaveNameAndPriority()
        dataProvider?.trySaveNotificationChanges()
        router?.popScreen()
    }
}
