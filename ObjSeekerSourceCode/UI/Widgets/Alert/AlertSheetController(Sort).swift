//
//  AlertSheetController(Sort).swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 15/02/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

final class AlertSortController {
    static func createAndShow(
        parent: UIViewController,
        alertType: AlertType,
        placeholder: PlaceholdController,
        title: String?,
        sortingArray: [NSSortDescriptor],
        sortBy: FilterEnumeration,
        additionalParameters: ((UITextField)->())? = nil,
        actionHandler: ((FilterEnumeration?)->())? = nil
    ) -> Void {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: alertType.styleGenerator)

        sortBy.caseArray.enumerated().forEach{(e) in
            let alertAction = UIAlertAction(title: e.element.label, style: .default) { (_) in
                let indexCase = sortBy.caseArray[e.offset]
                actionHandler?(indexCase)
            }
            alert.addAction(alertAction)
        }
        
        let cancelAction = UIAlertAction(title: S.Alert.Cancel, style: .cancel)
        alert.addAction(cancelAction)
        
        parent.present(alert, animated: true)
    }
}
