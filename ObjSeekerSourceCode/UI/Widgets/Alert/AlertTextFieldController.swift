//
//  AlertRenameController.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 15/02/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

final class AlertTextFieldController {
    static func createAndShow(
        parent: UIViewController,
        alertType: AlertType,
        placeholder: PlaceholdController,
        title: String?,
        additionalParameters: ((UITextField)->())? = nil,
        actionHandler: ((String?)->())? = nil
    ) -> Void {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: alertType.styleGenerator)
        alert.addTextField { textField in
            textField.autocorrectionType = .default
            textField.clearButtonMode = .always
            textField.spellCheckingType = .default
            textField.autocapitalizationType = .sentences
            textField.placeholder = placeholder.placeholderLabel
            additionalParameters?(textField)
        }
        
        let cancelAction = UIAlertAction(title: S.Alert.Cancel, style: .cancel, handler: nil)
        
        let saveAction = UIAlertAction(title: S.Alert.Done, style: .default) { [weak alert] _ in
            guard let text = alert?.textFields?.first?.text else { return }
            actionHandler?(text)
        }
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        parent.present(alert, animated: true)
    }
}
