//
//  AppDelegate.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 11/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigation: UINavigationController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Show tutorial to new user
        
//        if !(UserDefaults.standard.bool(forKey: "Tutorial was see")) {
//            let navigationController = window?.rootViewController as? UINavigationController
//            let tutorialView: UIViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController")
//            navigationController?.pushViewController(tutorialView, animated: true)
//        }
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        navigation = NavigationCore()
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.instance.saveContext()
    }
}
