
//  ReminderManager.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 24/01/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import EventKit
import Foundation

class ReminderManager {
    
    static let instance = ReminderManager()
    private let eventStore = EKEventStore() // Хранилище отвечающиее за все события из календаря (включая события и напоминания)
    
    private init() {}
    
    /// Первым делом проверяем доступ к календарю, без этого напоминания не будут работать
    private func accessToReminders() {
        EKEventStore().requestAccess(to: EKEntityType.reminder) { (granted: Bool, error: Error?) -> Void in
            if granted == false {
                print("\(self): \(#function) The app is not permitted to access reminders, make sure to grant permission in the settings and try again")
            }
        }
    }
    
    /// Устанавливаем доступ к календарю (Ключевое место в создании системных напоминаний)
    public func setCalendarAccess() {
        let status = EKEventStore.authorizationStatus(for: EKEntityType.reminder)
        switch (status) {
        case EKAuthorizationStatus.notDetermined:
            // This happens on first-run
            print("\(self): \(#function) Ask user for permission")
            ReminderManager.instance.accessToReminders()
        case EKAuthorizationStatus.authorized:
            // Things are in line with being able to show the calendars in the table view
            print("\(self): \(#function) Access to user calendar Granted")
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
            // We need to help them give us permission
            print("\(self): \(#function) Access to user calendar restricted or denied")
        @unknown default:
            print("\(self): \(#function) Unknown default EKEventStore.authorizationStatus")
        }
    }
    
    /// Проверяем статус доступа к календарю
    public func checkCalendarAccess() -> Bool {
        let status = EKEventStore.authorizationStatus(for: EKEntityType.reminder)
        switch (status) {
        case EKAuthorizationStatus.authorized:
            // Things are in line with being able to show the calendars in the table view
            return true
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied, EKAuthorizationStatus.notDetermined:
            // We need to help them give us permission
            print("\(self): \(#function) Access to user calendar restricted or denied or notDetermined")
            return false
        @unknown default:
            print("\(self): \(#function) Unknown default EKEventStore.authorizationStatus")
            return false
        }
    }
    
    /// Получаем календарь для работы
    private func getCalendar() -> EKCalendar {
        let calendarIdentifier = UserDefaults.standard.value(forKey: StcStr.UserDefaults.calendarString) as? String ?? "error"
        var calendar = ReminderManager.instance.eventStore.calendar(withIdentifier: calendarIdentifier)
        if calendar == nil {
            let newCalendar = EKCalendar(for: .reminder, eventStore: eventStore)
            newCalendar.title = StcStr.UserDefaults.calendarName
            newCalendar.source = eventStore.defaultCalendarForNewReminders()?.source
            do {
                try eventStore.saveCalendar(newCalendar, commit: true)
                UserDefaults.standard.set(newCalendar.calendarIdentifier, forKey: StcStr.UserDefaults.calendarString)
                calendar = newCalendar
            } catch {
                print(error)
            }
        }
        return calendar!
    }
    
    /// Создаем напоминание
    public func createReminder(title: String, isCompleted: Bool, dueDateComponents: DateComponents?, dueDate: Date, completionDate: Date?, priority: Int) -> EKReminder {
        let reminder = EKReminder(eventStore: eventStore)
        reminder.title = title // EKReminder required!!!!
        reminder.calendar = ReminderManager.instance.getCalendar() // EKReminder required!!!!
        reminder.priority = priority // Take from Slider
        reminder.isCompleted = isCompleted // Take from CoreData Task or SubTask
        reminder.dueDateComponents = dueDateComponents // Take from DatePicker
        reminder.startDateComponents = ReminderManager.instance.generateDateComponents(taskDate: Date()) // Gives when reminder create
        reminder.completionDate = completionDate // If isCompleted = true, completionDate set automatic
        reminder.addAlarm(ReminderManager.instance.generateAlarm(dueDate: dueDate))
        return reminder
    }
    
    /// Добавить новое напоминание в систему
    public func addReminder(userReminder: EKReminder) -> String? {
        do {
            try eventStore.save(userReminder, commit: true)
            print("\(self): \(#function) Add Reminder to Calendar complete")
            print("\(self): \(#function) Reminder Identifier: \(userReminder.calendarItemIdentifier)")
            return userReminder.calendarItemIdentifier
        } catch {
            print(error)
            return nil
        }
    }
    
    /// Удалить установленное напоминание из системы
    public func removeReminder(userReminder: EKReminder) {
        do {
            try eventStore.remove(userReminder, commit: true)
        } catch {
            print(error)
        }
    }
    
    /// Создаем будильник для события
    private func generateAlarm(dueDate: Date) -> EKAlarm {
        return EKAlarm.init(absoluteDate: dueDate)
    }
    
    /// Создаем компоненты даты, для работы некоторых функций (Reminder, Calendar)
    public func generateDateComponents(taskDate: Date) -> DateComponents {
        var result = DateComponents()
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute]
        result = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: taskDate)
        result.year = taskDate.year
        result.month = taskDate.month
        result.day = taskDate.day
        result.hour = taskDate.hour
        result.minute = taskDate.minute
        return result
    }
    
    public func getCalendarItem(reminderIdentifier: String?) -> EKReminder? {
        if (reminderIdentifier != nil) && checkCalendarAccess() {
            return eventStore.calendarItem(withIdentifier: reminderIdentifier!) as? EKReminder
        } else { return nil }
    }
}
