//
//  CoreDataManager.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 11/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    static let instance = CoreDataManager()
    static let sortKey = [
                            NSSortDescriptor(key: "isAchieved", ascending: true),
                            NSSortDescriptor(key: "nameString", ascending: true),
                            NSSortDescriptor(key: "priority", ascending: true),
                         ]
    
    private init() {}
    
    /// Основной контроллер, с помощью которого производятся изменения tableView. Содержит в себе информацию о задачах и подзадачах
    func fetchedTaskController(
        entityName: String? = "Task",
        arrayForSort: [NSSortDescriptor],
        predicate: NSPredicate? = nil
    ) -> FetchedTasks {
        let fetchRequest = NSFetchRequest<Task>(entityName: entityName!)
        fetchRequest.sortDescriptors = arrayForSort
        fetchRequest.predicate = predicate
        
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: persistentContainer.viewContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        return fetchedResultsController
    }

    // Entity for Name
    /// Возвращает указатель на сущность по запросу имени
    func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: persistentContainer.viewContext)!
    }
    
    /// Проверка выполнения цели потомков у определенного родителя
    func checkAchievParent(withTask task: Task) -> Bool {
        var result = true // Результат проверки
        var childrenSubTasks = [SubTasks]() // Массив с потомками (SubTask) после запроса в хранилище
        childrenSubTasks = SubTasks.getSubTaskForTask(identifier: task).fetchedObjects ?? []
        if childrenSubTasks.isEmpty { return false }
        
        for children in childrenSubTasks {
            if children.isAchieved == false { result = false }
        }
        return result
    }
    
    /// Выносим проведение performFetch из блоков контроллеров в CoreDataManager (для меньшего количества кода)
    func fetchingData(requestController: FetchedResult) -> FetchedResult {
        do {
            try requestController.performFetch()
        } catch {
            print(error)
        }
        return requestController
    }
    
    /// Добавление значения в сущность
    func addNewValue<key>(Value value: key, Entity entity: String, KeyPath keyPath: String) {
        let managedObject = persistentContainer.viewContext
        guard let managedEntity = NSEntityDescription.entity(forEntityName: entity, in: managedObject) else { return }
        let taskContainer = NSManagedObject(entity: managedEntity, insertInto: managedObject)
        taskContainer.setValue(value, forKey: keyPath)
    }
    
    func deleteObject(at indexPath: IndexPath, from controller: FetchedResult) {
        let viewContext = persistentContainer.viewContext
        let managedObject = controller.object(at: indexPath) as! NSManagedObject
        viewContext.delete(managedObject)
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ObjSeeker")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("Save context done")
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
