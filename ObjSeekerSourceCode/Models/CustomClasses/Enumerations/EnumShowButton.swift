//
//  Enums.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 09/02/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol Togglable {
    mutating func toggle()
}

enum ShowingStatus: Togglable {
    case show, hide
    
    var label: String {
        switch self {
        case .show: return S.TabBar.Hide
        case .hide: return S.TabBar.Show
        }
    }
    
    mutating func toggle() {
        switch self {
        case .show:
            self = .hide
        case .hide:
            self = .show
        }
    }
    
    func validate() -> NSPredicate? {
        switch self {
        case .show:
            return nil
        case .hide:
            return NSPredicate(format: "isAchieved == %@", NSNumber(value: false))
        }
    }
}
