//
//  EnumSortDescriptors.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 11/02/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

enum FilterEnumeration: String, CaseIterable {
    case isAchieved
    case nameString
    case priority
    
    var label: String {
        switch self {
        case .isAchieved: return S.Sheet.CompletedTasks
        case .nameString: return S.Sheet.Name
        case .priority: return S.Sheet.Priority
        }
    }
    var count: Int { get {
        return FilterEnumeration.allCases.count
        } }
    
    var caseArray: [FilterEnumeration] { get {
        return FilterEnumeration.allCases
        } }
    
    init (_: String) {
        self = .nameString
    }
    
    private func findDescriptor(descriptors: [NSSortDescriptor]) -> Int {
        if let descriptorIndex = descriptors.firstIndex(of: sortDescriptor(sortKey: self, ascending: true)) {
            return descriptorIndex
        } else { return descriptors.firstIndex(of: sortDescriptor(sortKey: self, ascending: false))! }
    }
    
    private func sortDescriptor(sortKey: FilterEnumeration, ascending: Bool) -> NSSortDescriptor {
        return NSSortDescriptor(key: sortKey.rawValue, ascending: ascending)
    }
    
    private func changeAscending(oldValue: NSSortDescriptor, sortKey: FilterEnumeration) -> NSSortDescriptor {
        let ascending = !(oldValue.ascending)
        return sortDescriptor(sortKey: sortKey, ascending: ascending)
    }
    
    private func changePositionInArray(descriptors: [NSSortDescriptor]) -> [NSSortDescriptor] {
        var result = descriptors
        if findDescriptor(descriptors: descriptors) == 1 {
            result[1] = changeAscending(oldValue: result[1], sortKey: self)
        } else {
            let elementIndexOld = findDescriptor(descriptors: descriptors)
            let elementToChange = result[elementIndexOld]
            result.remove(at: elementIndexOld)
            result.insert(elementToChange, at: 1)
        }
        return result
    }
    
    func finalSort(descriptors: [NSSortDescriptor]?) -> [NSSortDescriptor] {
        var result = [NSSortDescriptor]()
        if var oldDescriptors = descriptors {
            switch self {
            case .isAchieved:
                oldDescriptors[0] = changeAscending(oldValue: oldDescriptors[0], sortKey: self)
                return oldDescriptors
            default:
                return changePositionInArray(descriptors: oldDescriptors)
            }
        } else {
            result.append(sortDescriptor(sortKey: .isAchieved, ascending: true))
            result.append(sortDescriptor(sortKey: .nameString, ascending: true))
            result.append(sortDescriptor(sortKey: .priority, ascending: true))
        }
        return result
    }
}
