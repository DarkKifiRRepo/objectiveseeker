//
//  EnumAlertController.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 15/02/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

enum AlertType {
    case sheet
    case alert
    
    var styleGenerator: UIAlertController.Style {
        switch self {
        case .sheet:
            return UIAlertController.Style.actionSheet
        case .alert:
            return UIAlertController.Style.alert
        }
    }
}

enum PlaceholdController {
    case tvTask
    case tvSubTask
    
    var placeholderLabel: String {
        switch self {
        case .tvTask:
            return S.TFields.TaskName
        case .tvSubTask:
            return S.TFields.SubTaskName
        }
    }
}
