//
//  typeProtocol.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 29/01/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol TaskProtocol {
    /// Возвращает задачу/подзадачу
    var payload: TaskClass { get }
//    mutating func compareAchievedTaskAndReminder()
}

enum TaskClass: Equatable {
    case task(Task)
    case subtask(SubTasks)
    
    // MARK: Getters
    
    /// getter: завершена ли задача
    var isAchieved: Bool {
        switch self {
        case let .subtask(s):
            return s.isAchieved
        case let .task(t):
            return t.isAchieved
        }
    }
    
    /// getter: имя задачи
    var nameString: String {
        switch self {
        case let .subtask(s):
            return s.nameString
        case let .task(t):
            return t.nameString
        }
    }
    
    /// getter: идентификатор напоминания
    var deadlineIdentifier: String? {
        switch self {
        case let .subtask(s):
            return s.deadlineIdentifier
        case let .task(t):
            return t.deadlineIdentifier
        }
    }
    
    /// getter: дата напоминания
    var deadlineDate: Date? {
        switch self {
        case let .subtask(s):
            return s.deadlineDate
        case let .task(t):
            return t.deadlineDate
        }
    }
    
    ///  getter: приоритет
    var priority: Int32 {
        switch self {
        case let .subtask(s):
            return s.priority
        case let .task(t):
            return t.priority
        }
    }
    
    /// getter: время в виде строки
    var date: String? {
        switch self {
        case let .subtask(s):
            return s.dateForLabel
        case let .task(t):
            return t.dateForLabel
        }
    }
    
    // MARK: Setters
    
    /// setter: установить имя
    func setName(_ name: String) {
        switch self {
        case let .subtask(s):
            s.nameString = name
        case let .task(t):
            t.nameString = name
        }
    }
    
    /// setter: установить выполненность задачи
    func setAchieved(_ isDone: Bool) {
        switch self {
        case let .subtask(s):
            s.isAchieved = isDone
        case let .task(t):
            t.isAchieved = isDone
        }
    }
    
    /// setter: установить идентификатор напоминания
    func setDeadlineIdentifier(_ id: String?) {
        switch self {
        case let .subtask(s):
            s.deadlineIdentifier = id
        case let .task(t):
            t.deadlineIdentifier = id
        }
    }
    
    /// setter: установить приоритет
    func setPriority(_ priority: Float) {
        switch self {
        case let .subtask(s):
            s.priority = Int32(priority)
        case let .task(t):
            t.priority = Int32(priority)
        }
    }
}
