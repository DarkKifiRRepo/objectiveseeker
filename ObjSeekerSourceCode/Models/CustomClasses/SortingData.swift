//
//  sortingData.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 06.06.2021.
//  Copyright © 2021 com.Evseev. All rights reserved.
//

import Foundation

struct SortingData {
    let descriptors: [NSSortDescriptor]
    let currentState: FilterEnumeration
    
    init(state: FilterEnumeration = .nameString, array: [NSSortDescriptor]?) {
        currentState = state
        descriptors = state.finalSort(descriptors: array)
    }
}
