//
//  CustomView.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 21/09/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class CustomView {
    enum State {
        case done
        case undone
    }
    
    var view: UIView
    private var state: State
    
    init(_ cornerRadius: Int, state: State) {
        self.view = UIView()
        self.view.layer.cornerRadius = CGFloat(cornerRadius)
        CustomView.toogleState(&self.view, state)
        self.state = state
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toogleAcomplish(_ state: State) {
        CustomView.toogleState(&self.view, state)
    }
}

extension CustomView {
    fileprivate static func toogleState(_ view: inout UIView,_ state: State) {
        switch state {
        case .done:
            view.borderColor = Style.UserColors.borderForCells
            view.backgroundColor = Style.UserColors.cellDoneColor
        case .undone:
            view.borderColor = Style.UserColors.borderForCells
            view.backgroundColor = Style.UserColors.cellUndoneColor
        }
    }
}

extension UIEdgeInsets {
    init(all: CGFloat) {
        self.init(top: all, left: all, bottom: all, right: all)
    }
    
    init(horizontal: CGFloat, vertical: CGFloat) {
        self.init(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
    }
}
