//
//  LayersForViews.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 01/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

struct LayerForViews {}

extension LayerForViews {
    static func gradiented(toView: UIView, colors: [CGColor]) -> CAGradientLayer {
        let gradiented = CAGradientLayer()
        gradiented.startPoint = CGPoint(x: 0.5, y: 0)
        gradiented.endPoint = CGPoint(x: 0.5, y: 1)
        gradiented.colors = colors
        gradiented.frame = toView.bounds
        return gradiented
    }
}
