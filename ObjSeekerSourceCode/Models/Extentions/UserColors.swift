//
//  UserColors.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 18/08/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

enum Style {}

extension Style {
    /// Пользовательские цвета для элементов графического интерфейса
    struct UserColors {
        /// Цвет заливки ячейки с невыполненной задачей
        static let cellUndoneColor: UIColor = #colorLiteral(red: 0.9607843137, green: 0.9882352941, blue: 1, alpha: 1)
        /// Часть цвета заднего фона для градиентной заливки
        static let bgTop: UIColor = #colorLiteral(red: 0.8666666667, green: 0.8784313725, blue: 0.9921568627, alpha: 1)
        /// Часть цвета заднего фона для градиентной заливки
        static let bgBottom: UIColor = #colorLiteral(red: 0.2392156863, green: 0.3843137255, blue: 0.7960784314, alpha: 1)
        /// Часть цвета заднего фона для градиентной заливки
        static let bgMiddle: UIColor = #colorLiteral(red: 0.4078431373, green: 0.5176470588, blue: 0.8470588235, alpha: 1)
        /// Цвет текста для не выполненных задач
        static let textColor: UIColor = #colorLiteral(red: 0.1254901961, green: 0.1254901961, blue: 0.1254901961, alpha: 1)
        /// Цвет границы ячеек
        static let borderForCells: UIColor = #colorLiteral(red: 0.2599948943, green: 0.4010428786, blue: 0.2172519863, alpha: 1)
        /// Цвет заливки ячейки с выполненной задачей
        static let cellDoneColor: UIColor = #colorLiteral(red: 0.2823529412, green: 0.5019607843, blue: 0.1058823529, alpha: 1)
        /// Цвет текста для выполненной задачи
        static let textComplete: UIColor = #colorLiteral(red: 0.09411764706, green: 0.2666666667, blue: 0.04705882353, alpha: 1)
    }
    
    struct UserFonts {
        /// Шрифт задач
        static let taskMainFont: UIFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
        /// Шрифт времени в задачах
        static let taskAddFont: UIFont = UIFont.systemFont(ofSize: 12, weight: .regular)
        /// Шрифт в ячейках подзадач
        static let collectionFont: UIFont = UIFont.systemFont(ofSize: 10, weight: .regular)
        /// Шрифт в футере таблицы
        static let footerCellFont: UIFont = UIFont.systemFont(ofSize: 24, weight: .medium)
        /// Шрифт на экране деталей задачи
        static let detailViewFont: UIFont = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
}
