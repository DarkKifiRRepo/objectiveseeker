//
//  SubTasks+CoreDataProperties.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 19/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//
//

import Foundation
import CoreData


extension SubTasks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SubTasks> {
        return NSFetchRequest<SubTasks>(entityName: "SubTasks")
    }
    
    /// Выполнена ли задача (зависит от дочерних задач)
    @NSManaged public var isAchieved: Bool
    /// Имя задачи
    @NSManaged public var nameString: String
    /// Срок выполнения задачи (устанавливает при иннициализации напоминания)
    @NSManaged public var deadlineIdentifier: String?
    /// Приоритет задачи (для сортировки)
    @NSManaged public var priority: Int32
    /// Родиельская задача (может быть только одна)
    @NSManaged public var parent: Task
}

extension SubTasks: TaskProtocol {
    var payload: TaskClass {
        return .subtask(self)
    }
    
    /// Строка даты, которая получается из DateComponents. Нужно для вывода на экран
    var dateForLabel: String? {
        guard let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier) else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy 'г., ' HH:mm"
        return dateFormatter.string(from: dependReminder.dueDateComponents!.date!)
    }
    
    /// Даты завершения события
    var deadlineDate: Date? {
        guard
            let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier)
        else { return nil }
        
        return dependReminder.dueDateComponents?.date
    }
    
    /// Данная функция класса нам необходима для вывода списка подзадач в соответствии с выбранной задачей (посылается запрос с фильтром в хранилище) - Возвращает контроллер
    class func getSubTaskForTask(identifier parent: Task) -> FetchedSubTasks {
        let fetchRequest = NSFetchRequest<SubTasks>(entityName: "SubTasks")
        fetchRequest.sortDescriptors = CoreDataManager.sortKey
        fetchRequest.predicate = NSPredicate(format: "%K == %@", "parent", parent)
        
        var fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataManager.instance.persistentContainer.viewContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        fetchedResultsController = CoreDataManager.instance.fetchingData(requestController: fetchedResultsController as! FetchedResult) as! FetchedSubTasks
        return fetchedResultsController
    }

    /// Проверка на выполненность задачи из приложения "Напоминания"
    func compareAchievedTaskAndReminder() {
        if let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier) {
            if dependReminder.completionDate != nil {
                isAchieved = true
                CoreDataManager.instance.saveContext()
                print("\(self): \(#function) Depend Reminder of Task is Complete and isAchieved of Task Changed to True")
            }
        }
    }
}
