//
//  SubTasks+CoreDataClass.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 19/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(SubTasks)
public class SubTasks: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "SubTasks"), insertInto: CoreDataManager.instance.persistentContainer.viewContext)
    }
}
