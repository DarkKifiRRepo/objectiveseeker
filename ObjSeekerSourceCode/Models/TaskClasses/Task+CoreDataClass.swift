//
//  Task+CoreDataClass.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 11/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Task)
public class Task: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Task"), insertInto: CoreDataManager.instance.persistentContainer.viewContext)
    }
}
