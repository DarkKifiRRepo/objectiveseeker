//
//  AppTask.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 25.01.2020.
//  Copyright © 2020 com.Evseev. All rights reserved.
//

import Foundation

struct AppTask: Equatable {
    var isAchieved: Bool
    var name: String
    var deadlineIdentifier: String?
    var priority: Int
    var taskClass: TaskClass

    indirect enum TaskClass: Equatable {
        case child
        case parent([AppTask])
    }
    
    static func fillTasks(tasks: [TaskAliasProtocol]) -> [AppTask] {
        if let appSubtask = tasks as? [SubTasks] {
            return appSubtask.map{ $0.asAppTask() }
        } else {
            let appTask = tasks as! [Task]
            return appTask.map{ $0.asAppTask() }
        }
    }
    
    mutating func update(with updFunc: (inout AppTask) -> ()) {
        updFunc(&self)
    }
    
    mutating func add(child: AppTask) {
        if child.taskClass == .child { return }
        update(with: { task in
            switch task.taskClass {
            case var .parent(children):
                children.append(child)
                task.taskClass = .parent(children)
            case .child:
                return
            }
        })
    }
}

extension AppTask {
    static func serialize(tasks: [TaskAliasProtocol]) -> ([Task], [SubTasks]) {
        
        return ([], [])
    }
}
