//
//  Task+CoreDataProperties.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 19/12/2018.
//  Copyright © 2018 com.Evseev. All rights reserved.
//
//

import Foundation
import CoreData

extension Task {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }
    /// Выполнена ли задача (зависит от дочерних задач)
    @NSManaged public var isAchieved: Bool
    /// Имя задачи
    @NSManaged public var nameString: String
    /// Срок выполнения задачи (устанавливает при иннициализации напоминания)
    @NSManaged public var deadlineIdentifier: String?
    /// Дочерние задачи (может быть несколько)
    @NSManaged public var children: NSSet?
    /// Приоритет задачи (для сортировки)
    @NSManaged public var priority: Int32
}

// MARK: Generated accessors for children
extension Task {
    @objc(addChildrenObject:)
    @NSManaged public func addToChildren(_ value: SubTasks)

    @objc(removeChildrenObject:)
    @NSManaged public func removeFromChildren(_ value: SubTasks)

    @objc(addChildren:)
    @NSManaged public func addToChildren(_ values: NSSet)

    @objc(removeChildren:)
    @NSManaged public func removeFromChildren(_ values: NSSet)
}

extension Task: TaskProtocol {
    var payload: TaskClass {
        return .task(self)
    }
    
    /// Даты завершения события
    var deadlineDate: Date? {
        guard
            let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier)
        else { return nil }
        
        return dependReminder.dueDateComponents?.date
    }
    
    /// Строка даты, которая получается из DateComponents. Нужно для вывода на экран
    var dateForLabel: String? {
        if let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yy 'г., ' HH:mm"
            return dateFormatter.string(from: (dependReminder.dueDateComponents?.date)!)
        }
        return nil
    }

    /// Проверка на выполненность задачи из приложения "Напоминания"
    func compareAchievedTaskAndReminder() {
        if let dependReminder = ReminderManager.instance.getCalendarItem(reminderIdentifier: deadlineIdentifier) {
            if dependReminder.completionDate != nil {
                isAchieved = true
                CoreDataManager.instance.saveContext()
                print("\(self): \(#function) Depend Reminder of Task is Complete and isAchieved of Task Changed to True")
            }
        }
    }
}
