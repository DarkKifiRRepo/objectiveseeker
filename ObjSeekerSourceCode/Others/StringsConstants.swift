//
//  StringsConstants.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 04.03.2020.
//  Copyright © 2020 com.Evseev. All rights reserved.
//

import Foundation

/// Static Strings
enum StcStr {
    /// User Defaults
    enum UserDefaults {
        static let calendarName = "DoneItApp"
        static let calendarString = "DoneItCalendar"
    }
    
    enum IconsNames {
        static let sortIconName = "Sort icon"
        static let addIconName = "Add Icon"
        static let editIconName = "EditIcon"
        static let deleteIconName = "DeleteIcon"
    }
    
    /// Notifcation
    enum Notif {
        /// Detail View
        struct DV {
            /// switcher_detail_action_tapped
            static let addReminder = "switcher_detail_action_tapped"
            /// label_detail_action_tapped
            static let label = "label_detail_action_tapped"
            /// save_detail_action_tapped
            static let saveButton = "save_detail_action_tapped"
            /// data_transfer_object_changed
            static let dObj = "data_transfer_object_changed"
        }
        
        /// Task List View
        enum TLV {
            /// detail_task_action_tapped
            static let detailAction = "detail_task_action_tapped"
            /// delete_task_action_tapped
            static let deleteAction = "delete_task_action_tapped"
            /// "add_task_action_tapped"
            static let addAction = "add_task_action_tapped"
            /// sort_task_action_tapped
            static let sortAction = "sort_task_action_tapped"
            /// subtask_task_action_tapped
            static let subtaskAction = "subtask_task_action_tapped"
        }
        
        /// Subtask List View
        enum SLV {
            /// detail_subtask_action_tapped
            static let detailAction = "detail_subtask_action_tapped"
            /// delete_subtask_action_tapped
            static let deleteAction = "delete_subtask_action_tapped"
            /// add_subtask_action_tapped
            static let addAction = "add_subtask_action_tapped"
            /// sort_subtask_action_tapped
            static let sortAction = "sort_subtask_action_tapped"
        }
    }
}
