//
//  ImageConstants.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 12.06.2021.
//  Copyright © 2021 com.Evseev. All rights reserved.
//

import UIKit

/// Static Images
enum StcImage {
    static let addIcon = UIImage(named: StcStr.IconsNames.addIconName)
    static let deleteIcon = UIImage(named: StcStr.IconsNames.deleteIconName)
    static let sortIcon = UIImage(named: StcStr.IconsNames.sortIconName)
    static let editIcon = UIImage(named: StcStr.IconsNames.editIconName)
}
