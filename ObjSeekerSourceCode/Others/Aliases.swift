//
//  aliases.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 12.06.2021.
//  Copyright © 2021 com.Evseev. All rights reserved.
//

import CoreData
import Foundation

typealias FetchedResult = NSFetchedResultsController<NSFetchRequestResult>
typealias FetchedTasks = NSFetchedResultsController<Task>
typealias FetchedSubTasks = NSFetchedResultsController<SubTasks>
