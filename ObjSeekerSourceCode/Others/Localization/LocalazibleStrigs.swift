//
//  LocalazibleStrigs.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 17/08/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

/// Строки локализованные под языки для элеменов графического интерфейса
struct S {
    /// форма Alert
    struct Alert {
        /// "Done" = "Готово";
        static let Done = NSLocalizedString("Done", comment: "")
        /// "Ok" = "Окей";
        static let Ok = NSLocalizedString("Ok", comment: "")
        /// "Yes" = "Да";
        static let Yes = NSLocalizedString("Yes", comment: "")
        /// "Cancel" = "Отмена";
        static let Cancel = NSLocalizedString("Cancel", comment: "")
        /// "New Task" = "Новая задача";
        static let NewTask = NSLocalizedString("New Task", comment: "")
        /// "New Subtask" = "Новая подзадача";
        static let NewSubTask = NSLocalizedString("New Subtask", comment: "")
        /// "Menu" = "Меню изменений";
        static let Menu = NSLocalizedString("Menu", comment: "")
        /// "Rename task" = "Переименовать задачу";
        static let RenameTask = NSLocalizedString("Rename task", comment: "")
        /// "Rename Subtask" = "Переименовать подзадачу";
        static let RenameSubtask = NSLocalizedString("Rename Subtask", comment: "")
        /// "Subtasks table Error" = "Ошибка формирования таблицы";
        static let STError = NSLocalizedString("Subtasks table Error", comment: "")
        /// "Error" = "Переустановите приложение";
        static let TError = NSLocalizedString("Error", comment: "")
    }
    
    /// Кнопка показа и сокрытия выполненных задач
    struct TabBar {
        /// "Show complete task" = "Не завершенные";
        static let Show = NSLocalizedString("Show complete task", comment: "")
        /// "Hide complete task" = "Все задачи";
        static let Hide = NSLocalizedString("Hide complete task", comment: "")
    }
    
    /// Меню навигации
    struct NavBar {
        /// "Task List" = "Задачи";
        static let TaskList = NSLocalizedString("Task List", comment: "")
        /// "Subtask List" = "Подзадачи";
        static let SubTaskList = NSLocalizedString("Subtask List", comment: "")
        /// "Details" = "Подробности";
        static let DetailView = NSLocalizedString("Details", comment: "")
    }
    
    /// Меню сортировки
    struct Sheet {
        /// "Sort by" = "Сортировать";
        static let SortBy = NSLocalizedString("Sort by", comment: "")
        /// "Completed tasks" = "по выполненности задач";
        static let CompletedTasks = NSLocalizedString("Completed tasks", comment: "")
        /// "Name" = "по имени";
        static let Name = NSLocalizedString("Name", comment: "")
        /// "Priority" = "по приоритету";
        static let Priority = NSLocalizedString("Priority", comment: "")
    }
    
    /// Действия по свайпу на ячейке
    struct Actions {
        /// "Delete?" = "Удалить?";
        static let Delete = NSLocalizedString("Delete?", comment: "")
        /// "Edit" = "Изменить";
        static let Edit = NSLocalizedString("Edit", comment: "")
    }
    
    /// Текстовые поля в Alert
    struct TFields {
        /// "Enter Task Name" = "Введите имя задачи";
        static let TaskName = NSLocalizedString("Enter Task Name", comment: "")
        /// "Enter Subtask Name" = "Введите имя подзадачи";
        static let SubTaskName = NSLocalizedString("Enter Subtask Name", comment: "")
    }
    
    /// Меню из Alert, при вызове по зажатию пальцем на ячейке
    struct AMenu {
        /// "Done Task" = "Завершить задачу";
        static let Done = NSLocalizedString("Done Task", comment: "")
        /// "Add New Subtask" = "Добавить новую подзадачу";
        static let Add = NSLocalizedString("Add New Subtask", comment: "")
    }
    
    struct Other {
        /// "Help" = "Помощь";
        static let Help = NSLocalizedString("Help?", comment: "")
        
        static let EmptyString = ""
    }
    
    /// Detail View
    struct DView {
        /// "Notification Time:" = "Время напоминания:";
        static let NotificationTime = NSLocalizedString("Notification Time:", comment: "")
        /// "Add Notification" = "Добавить напоминание";
        static let NotificationAdd = NSLocalizedString("Add Notification", comment: "")
        /// "Save" = "Сохранить";
        static let SaveButton = NSLocalizedString("Save", comment: "")
        /// "min" = "мин";
        static let min = NSLocalizedString("min", comment: "")
        /// "max" = "макс";
        static let max = NSLocalizedString("max", comment: "")
    }
}
