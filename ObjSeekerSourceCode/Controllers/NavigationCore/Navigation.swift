//
//  DataSourceNavigation.swift
//  ObjSeeker
//
//  Created by Александр Евсеев on 03/08/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

// TODO: подготовить навигатор к работе
// Должен уметь предоставлять данные экранам по запросу

protocol Navigation: AnyObject {
    func pushTaskScreen()
    func pushDetailScreen(task: TaskProtocol)
    func pushTutorialScreen()
    func pushSubTaskScreen(subTask: Task)
    func popScreen()
}

class NavigationCore: UINavigationController {
    var dataProvider: NavigationDataSource?
    
    enum viewType {
        case errorScreen
        case taskScreen
        case subTaskScreen(Task)
        case detailScreen(TaskProtocol)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        pushTaskScreen()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NavigationCore: Navigation {
    func pushTaskScreen() {
        self.pushViewController(generateView(Of: .taskScreen), animated: true)
    }
    
    func pushDetailScreen(task: TaskProtocol) {
        self.pushViewController(generateView(Of: .detailScreen(task)), animated: true)
    }
    
    func pushTutorialScreen() {
    }
    
    func pushSubTaskScreen(subTask: Task) {
        self.pushViewController(generateView(Of: .subTaskScreen(subTask)), animated: true)
    }
    
    func popScreen() {
        self.popViewController(animated: true)
    }
    
    func generateView(Of type: viewType) -> UIViewController {
        switch type {
        case .taskScreen:
            return NewTVTasks(dataProvider: TaskListDataProvider(), navigation: self)
        case let .detailScreen(task):
            return TVDetailTask(provider: DetailDataProvider(task: task, reminder: ReminderManager.instance), router: self)
        case let .subTaskScreen(task):
            return TVsubTask(provider: SubtasksDataProvider(initValue: task), router: self)
        case .errorScreen:
            // MARK: TODO: Заполнить кейс ErrorScreen
            print("\(self): \(#function) Broken generator to ErrorScreen")
            print("\(self): \(#function) Fatal error occupiced. Move to Error Screen")
            return UIViewController()
        }
    }
}
